'use strict'
const jsdom = require('jsdom');
const TelegramBot = require('node-telegram-bot-api');
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const serviceAccount = require("./config/conf.json");
admin.initializeApp( {
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://employment-bot.firebaseio.com",
});

let db = admin.firestore();

const { JSDOM } = jsdom;

const bot = new TelegramBot('744319824:AAHiYIRL5BB56KGD6JrXErr9ntiP1PsGi00', {
	webHook: {
		port: 3000,
		autoOpen: false
	}
});

bot.openWebHook()
bot.setWebHook(`https://api.telegram.org/bot${'744319824:AAHiYIRL5BB56KGD6JrXErr9ntiP1PsGi00'}/setWebhook`)

bot.on('message', msg => {
    const { chat: { id }} = msg;
    bot.sendMessage(id, 'да,я работаю!');
})

bot.onText(/\/help (.+)/, (msg, [source, match]) => {
    const { chat: { id } } = msg;
    bot.sendMessage(id, match);
})

bot.onText(/\/parse (.+)/, (msg, [source]) => {
    const { chat: { id } } = msg;
    // let vacsRef = db.collection('workuaWebscrap');  
    // let query = citiesRef.where('capital', '==', true).get()
    //     .then(snapshot => {
    //         if (snapshot.empty) {
    //             console.log('No matching documents.');
    //             return;
    //         }  

    //         snapshot.forEach(doc => {
    //             console.log(doc.id, '=>', doc.data());
    //         });
    //     })
    //     .catch(err => {
    //         console.log('Error getting documents', err);
    //     });
    bot.sendMessage(id, match);
})

// const getScript = (url) => {
//     return new Promise((resolve, reject) => {

//         let client = http;

//         if (url.toString().indexOf("https") === 0) {
//             client = https;
//         }

//         client.get(url, (resp) => {
//             let data = '';

//             // A chunk of data has been recieved.
//             resp.on('data', (chunk) => {
//                 data += chunk;
//             });

//             // The whole response has been received.
//             resp.on('end', () => {
//                 resolve(data);
//             });

//         }).on("error", (err) => {
//             reject(err);
//         });
//     });
// };

let dom = JSDOM.fromURL("https://www.work.ua/jobs-kyiv-it/?advs=1").then(dom => {
    for(let i of dom.window.document
        .getElementsByClassName("card card-hover card-visited wordwrap job-link js-hot-block"))
        {
            vacanciesParser(i);
        }
        return;
});

function vacanciesParser(content){
    const attr = (content.querySelector(".add-bottom-sm a") || {}).getAttribute('href');
    let idEl = attr.split('/')[2];
    console.log(idEl);
    const obj = {
        header: (content.querySelector(".add-bottom-sm") || {}).textContent,
        company: (content.querySelector("b") || {}).textContent,
        overflow: (content.querySelector(".overflow") || {}).textContent,
    };

    let colRef = db.collection('workuaWebscrap').doc(idEl);
    let setDoc = colRef.set({
        obj
    }).then(() => {
        console.log('Data saved');
        return 0;
    }).catch((e) => {
        console.log("FAILURE: ", e);
    });
    // fs.readFile('./tmp/jsonfile.json', 'utf8', function readFileCallback(err, data){
    //     if (err){
    //         console.log(err);
    //         return(0);
    //     } else {
    //         let obj = {
    //             table: [],
    //         };
    //         if(data){
    //             obj = JSON.parse(data);
    //         }
    //         obj.table.push({"header": header, "company":company, "overflow": overflow});
    //         let json = JSON.stringify(obj);
    //         fs.appendFile('./tmp/jsonfile.json', json+'\n', 'utf8', (err, data) => {
    //             if (err) {
    //                 console.log(err);
    //             }
    //         });
    //     }
    // });
}
// exports.sendMessage = functions.firestore
//     .document('workuaWebscrap/{scrapID}')
//     .onCreate(event => {    
//         const docID = event.params.scrapID;
//         const name = event.data.data().name;
//         const parseRef = admin.firestore().collection('workuaWebscrap').doc(docID);
//         return productRef.update({ message: `Nice ${name}! - Cloud functions worked. `});
// });